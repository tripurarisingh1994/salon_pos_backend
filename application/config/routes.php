<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']           = 'auth';
$route['404_override']                 = '';
$route['translate_uri_dashes']         = FALSE;
$route['logout']                       = 'auth/super_admin_logout';

$route['addClient']                    = 'admin/addClient';
$route['submit-client-details']        = 'admin/feedClientDetails';
$route['view-client']                  = 'admin/viewClientDetails';
$route['delete-client']                = 'admin/deleteClientDetails';
$route['update-client']                = 'admin/updateClientDetails';
$route['disable-client']               = 'admin/disableClient';

$route['view-user']                    = 'admin/viewUser';


$route['update-customer']              = 'common/updateCustomerDetails';
$route['delete-customer']              = 'common/deleteCustomerDetails';

$route['save-general-setting']         = 'common/saveGeneralSetting';
$route['getting-general-setting']      = 'common/fetchingGeneralSetting';

$route['create-new-item']              = 'common/createNewItem';
$route['update-item']                  = 'common/updateItemDetails';
$route['delete-item']                  = 'common/deleteItemDetails';

$route['fetch-appointment-details']    = 'common/fetchAppointmentDetails';

$route['update-food-category']         = 'common/updateFoodCategory';
$route['delete-food-category']         = 'common/deleteFoodCategory';
$route['create-food-category']         = 'common/createFoodCategory';

$route['get-user-details']             = 'common/gettingUsersDetails';
$route['create-user']                  = 'common/createUser';
$route['update-user']                  = 'common/updateUserDetails';
$route['delete-user']                  = 'common/deleteUserDetails';

$route['create_resource']              = 'common/createResources';
$route['update-resource']              = 'common/updateResources';
$route['delete-resource']              = 'common/deleteResources';

$route['create-staff']                 = 'common/createStaff';
$route['update-staff']                 = 'common/updateStaff';
$route['delete-staff']                 = 'common/deleteStaff';

$route['update-msg']                   = 'common/updateMsg';
$route['get-msg']                      = 'common/getMsg';

// Report Section
$route['sale-report']                  = 'common/getSaleReport';
$route['appointment-report']           = 'common/getAppointmentReport';


