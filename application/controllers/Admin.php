<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//header('Access-Control-Allow-Origin: *');

class Admin extends CI_Controller
{

    public function index()
    {
        $this->load->helper('url');

        if ($this->session->userdata('email')) {
            redirect('admin/dashboard');
        } else {
            $this->load->view('admin/admin');
        }
    }

    // dashboard
    public function dashboard()
    {
        if ($this->session->userdata('email')) {
            $this->load->view('admin/dashboard');
        } else {
            $this->load->view('admin/admin');
        }
    }

    // Add html code to Add Client Menu (side menu)
    public function addClient()
    {
        $output = '';

        $output .= '<button style="margin-top: 20px;" type="button" class="btn btn-primary" id="view-client-btn" class="txt2"> View Client </button>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form_main">
                                <h4 class="heading"><strong>Add </strong> Client <span></span></h4>
                                <div class="form">
                                <form id="contactFrm" name="contactFrm">
                                
                                    <input type="text"  placeholder="Client Name"   id="c-name"   class="txt form-control">
                                    <input type="text"  placeholder="Client Mobile" maxlength="10"  id="c-mob"    class="txt form-control">
                                    <input type="email" placeholder="Client Email" id="c-email"  class="txt form-control">
                                    <input type="email" placeholder="Sender Id" id="sender-id"  class="txt form-control">
                                    
                                    <div class="form-group">
                                      <label for="sel1">Account Status:</label>
                                      <select class="form-control" id="isactive">
                                        <option value="1" selected>Active</option>
                                        <option value="0">Inactive</option>
                                      </select>
                                    </div>
                                    
                                    <div class="form-group">
                                      <label for="sel1">Trial 30 Days:</label>
                                      <select class="form-control" id="trial-30">
                                        <option value="1" selected>Yes</option>
                                        <option value="0">No</option>
                                      </select>
                                    </div>
                                    
                                     <button type="button" class="btn btn-block btn-primary" id="submit" class="txt2"> Submit </button>
                                </form>
                            </div>
                            </div>
                    </div>
                </div>';


        echo $output;
    }


    // Feeding the client details in the db
    public function feedClientDetails()
    {

        $c_name = $this->input->post('c_name');
        $c_mob = $this->input->post('c_mob');
        $c_email = $this->input->post('c_email');
        $sender_id = $this->input->post('sender_id');
        $is_active = $this->input->post('is_active');
        $is_trial = $this->input->post('is_trial');


        $this->load->model('admin_model', 'am');

        $res = $this->am->saveClientDetails($c_name, $c_mob, $c_email, $sender_id, $is_active, $is_trial);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }

    }


    // View Client Details
    public function viewClientDetails()
    {
        $this->load->model('admin_model', 'am');
        $c_data = $this->am->fetch_client_details();


        $output = '';
        $output .='<button style="margin-top: 20px;" type="button" class="btn btn-primary" id="add-client-btn" class="txt2"> Add Client </button>
                       <table class="table">
                        <thead>
                          <tr>
                            <th>Client Name</th>
                            <th>Client Mobile</th>
                            <th>Client Email</th>
                            <th>Sender Id</th>
                            <th>Trial Plan</th>
                            <th>Active</th>
                            <th>Plan Start</th>
                            <th>Plan End</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>';

            if($c_data->num_rows() > 0) {

                foreach ($c_data->result() as $row) {

                    if($row->active == 0) {
                        $output .= '<tr style="border:3px solid red;">';
                    }
                    else {
                        $output .= '<tr>';
                    }
                    $output .= '<td><input class="form-control" id="c_name'.$row->id.'" type="text" value="'.$row->client_name.'" disabled></td>
                                    <td><input class="form-control" size="7" maxlength="10" id="c_mob'.$row->id.'"  type="text" value="'.$row->client_mob.'" disabled></td>
                                    <td><input class="form-control" id="c_email'.$row->id.'"  type="email" value="'.$row->client_email.'" disabled></td>
                                    <td><input class="form-control" size="4" id="sender_id'.$row->id.'"  type="text" value="'.$row->sender_id.'" disabled></td>
                                    <td><input class="form-control" size="1" id="is_trial'.$row->id.'"  type="text" value="'.$row->trial_plan.'" disabled></td>
                                    <td>'.$row->active.'</td>
                                    <td>'.$row->plan_start.'</td>
                                    <td>'.$row->plan_end.'</td>
                                    <td>
                                    <button type="button" id="edit'.$row->id.'" onclick="editClient('.$row->id.')" data-toggle="tooltip" title="Edit the client" class="btn btn-primary btn-xs"><i class="fas fa-edit"></i></button>
                                    <button type="button" onclick="deleteClient('.$row->id.')" data-toggle="tooltip" title="Delete the client" class="btn btn-danger btn-xs"><i class="fas fa-trash-alt"></i></button>';


                                    if($row->active == 0) {

                                        $output .= '<button style="margin-left:5px;" type="button" onclick="enable_or_disable_client('.$row->id.')" data-toggle="tooltip" title="Enable the client" class="btn btn-secondary btn-xs"><i class="fas fa-times"></i></i></button>';
                                    }
                                    else {
                                        $output .= '<button style="margin-left:5px;"  type="button" onclick="enable_or_disable_client('.$row->id.')" data-toggle="tooltip" title="Disable the client" class="btn btn-secondary btn-xs"><i class="fas fa-times"></i></i></button>';
                                    }


                             $output .= '</td>
                                        </tr>';
                }
            }
            else {
                $output .= ' <tr>
                                <td colspan="8">No Data Found</td>
                            </tr>';
            }

            $output .= '</tbody>
                      </table>';


             echo $output;
    }


    // Delete client details
    public function deleteClientDetails() {
        $id = $this->input->post('id');
        $this->load->model('admin_model', 'am');
        $chk = $this->am->delete_client_details($id);

        if ($chk == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }


    // update client details
    public function updateClientDetails() {
        $id = $this->input->post('id');
        $c_name = $this->input->post('c_name');
        $c_mob = $this->input->post('c_mob');
        $c_email = $this->input->post('c_email');
        $sender_id = $this->input->post('sender_id');
        $is_trial = $this->input->post('is_trial');


        $this->load->model('admin_model', 'am');

        $res = $this->am->update_ClientDetails($id, $c_name, $c_mob, $c_email, $sender_id, $is_trial);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }


    // Disable the client
    public function disableClient() {

        $id = $this->input->post('id');

        $this->load->model('admin_model', 'am');

        $res = $this->am->disable_client($id);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }

    }


    // view user details
    public function viewUser() {
        $this->load->model('admin_model', 'am');
        $u_data = $this->am->fetch_user_details();


        $output = '';
        $output .='<table class="table">
                        <thead>
                          <tr>
                            <th>Client Name</th>
                            <th>Email</th>
                            <th>Password</th>
                            <th>Active</th>
                            <th>Admin</th>
                          </tr>
                        </thead>
                        <tbody>';

        if(count($u_data) > 0) {

            foreach ($u_data as $row) {

                if($row['active'] == 0) {
                    $output .= '<tr style="border:3px solid red;">';
                }
                else {
                    $output .= '<tr>';
                }
                $output .= '<td><input class="form-control" id="c_name'.$row['id'].'" type="text" value="'.$row['client_name'].'" disabled></td>
                                    <td><input class="form-control" id="email'.$row['id'].'"  type="email" value="'.$row['email'].'" disabled></td>
                                    <td><input class="form-control" id="password'.$row['id'].'"  type="text" value="'.$row['password'].'" disabled></td>
                                    <td><input class="form-control" id="active'.$row['id'].'"  type="text" value="'.$row['active'].'" disabled></td></td>
                                    <td><input class="form-control" id="admin'.$row['id'].'"  type="text" value="'.$row['admin'].'" disabled></td></td>
                                    <td>
                                    <button type="button" id="edit'.$row['id'].'" onclick="editUser('.$row['id'].')" data-toggle="tooltip" title="Edit the client" class="btn btn-primary btn-xs"><i class="fas fa-edit"></i></button>
                                    <button type="button" onclick="deleteUser('.$row['id'].')" data-toggle="tooltip" title="Delete the user" class="btn btn-danger btn-xs"><i class="fas fa-trash-alt"></i></button>';


                if($row['active'] == 0) {

                    $output .= '<button style="margin-left:5px;" type="button" onclick="enable_or_disable_user('.$row['id'].')" data-toggle="tooltip" title="Enable the user" class="btn btn-secondary btn-xs"><i class="fas fa-times"></i></i></button>';
                }
                else {
                    $output .= '<button style="margin-left:5px;"  type="button" onclick="enable_or_disable_user('.$row['id'].')" data-toggle="tooltip" title="Disable the user" class="btn btn-secondary btn-xs"><i class="fas fa-times"></i></i></button>';
                }


                $output .= '</td>
                                        </tr>';
            }
        }
        else {

            $output .= ' <tr>
                                <td colspan="5">No Data Found</td>
                            </tr>';
        }

        $output .= '</tbody>
                      </table>';


        echo $output;

    }


}