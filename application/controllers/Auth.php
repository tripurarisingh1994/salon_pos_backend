<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//header('Access-Control-Allow-Origin: *');

class Auth extends CI_Controller {

    /************************* client login
     * *******************************************************/

	public function doLogin() {

        $this->load->model('auth_model','auth');
	    $email    = $this->input->post('email');
	    $password = base64_encode($this->input->post('password'));

        $res = $this->auth->admin_login($email, $password);

        if( $res->num_rows() == 1 ) {

            $row = $res->result_array(); // $res convert into associative array

            $last_login_row = $this->auth->last_login($row[0]['id'])->result_array();

            if(count($last_login_row) == 1 ) {

                $response = array(
                    'status'        =>      200,
                    'message'       =>      'success',
                    'client_id'     =>      (int) $last_login_row[0]['client_id'],
                    'client_name'     =>    $last_login_row[0]['client_name'],
                    'trial_plan'    =>      (int) $last_login_row[0]['trial_plan'],
                    'plan_start'    =>      $last_login_row[0]['plan_start'],
                    'plan_end'      =>      $last_login_row[0]['plan_end'],
                    'id'            =>      (int) $last_login_row[0]['id'],
                    'name'          =>      $last_login_row[0]['name'],
                    'email'         =>      $last_login_row[0]['email'],
                    'active'        =>      (int) $last_login_row[0]['active'],
                    'admin'         =>      (int) $last_login_row[0]['admin'],
                    'last_login'    =>      $last_login_row[0]['last_login']
                );

                echo json_encode($response);
            }

        }
        else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
  }


  /*********** Super Admin login (dashboard to handle client)
   ****************************************/

  public function super_admin() {

      $this->load->model('auth_model','auth');
      $this->load->library('session');

      $email    = $this->input->post('email');
      $password = base64_encode($this->input->post('password'));

      $res = $this->auth->super_admin_login($email, $password);

      if( $res->num_rows() == 1 ) {

          $row = $res->result_array();


          $login_data = array(
                                'id'     => $row[0]['id'],
                                'email'  => $row[0]['email']
                            );
          $this->session->set_userdata($login_data);

          $response = array(
              'status'        =>      200,
              'message'       =>      'success',
          );

          echo json_encode($response);
      } else {
          echo json_encode( array('status'=>'200','message'=>'fail') );
      }
  }

  /******************* Logout Super Admin
      *************************************************************************/

  public function super_admin_logout() {
      $login_data = array('id', 'email');
      $this->session->unset_userdata($login_data);

      redirect('index.php/admin', 'refresh');
  }

}
