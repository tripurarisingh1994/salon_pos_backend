<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//header('Access-Control-Allow-Origin: *');
header('Content-type: application/json; charset=utf-8');


class Common extends CI_Controller {

    /** Getting food-category */

    public function getFoodCategory() {
        $client_id = $this->input->get('client_id');
        $this->load->model('common_model', 'cm');

        $res = $this->cm->fetchFoodCategory($client_id);

        if ( $res->num_rows() > 0) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }



    // create the food category
    public function createFoodCategory() {
        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->create_food_category($data['client_id'], $data['cat_name']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }

    // Update Service Category (Food Category)
    public function updateFoodCategory() {

        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->update_food_category($data['client_id'], $data['cat_id'], $data['cat_name']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }


    // delete the food category
    public function deleteFoodCategory() {
        $data = json_decode(file_get_contents('php://input'), true);
        $this->load->model('common_model', 'cm');

        $res = $this->cm->delete_food_category($data['client_id'], $data['cat_id']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }

    /** Getting the Food Items List by Food Category Id */

    public function getFoodItemsByFoodCatId() {

        $client_id = $this->input->get('client_id');
        $id = $this->input->get('id');

        $this->load->model('common_model', 'cm');
        $res = $this->cm->fetchFoodItemsByFoodCatId($id, $client_id);

        if ( $res->num_rows()) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    /** Getting the price of food items by food id*/

    public function getFoodItemPriceByFoodItemId() {

        $client_id = $this->input->get('client_id');
        $id  = $this->input->get('id');

        $this->load->model('common_model', 'cm');
        $res = $this->cm->fetchFoodItemPriceByFoodItemId($id, $client_id);

        if ( $res->num_rows()) {
            $rows = $res->result_array();

            array_walk ( $rows, function (&$key) {
                $key['price'] = (int)$key['price']  + ((int)$key['price'] * (int)$key['gst_rate'])/100;
            } );

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    /** Saving the Customer Details */



    /** Searching the customer by customer name */
    public function searchCustomerByName() {

        $this->load->model('common_model', 'cm');
        $query     = $this->input->get('query');
        $client_id = $this->input->get('client_id');

        $res = $this->cm->fetchCustomerByName($client_id, $query);

        if ( $res->num_rows()) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    /**  Search food item by item name */

    public function searchFoodItemsByName() {
        $this->load->model('common_model', 'cm');
        $query = $this->input->get('query');
        $client_id = $this->input->get('client_id');

        $res = $this->cm->fetchFoodItemsByName($client_id, $query);

        if ( $res->num_rows()) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    /** Insert new Invoice data */
    public function insertInvoiceData() {

        $this->load->model('common_model', 'cm');
        $client_id       = $this->input->post('client_id');
        $customerId      = $this->input->post('customer_id');
        $amountReceive   = $this->input->post('amount_receive');
//        $tax             = $this->input->post('tax');
        $payment_mode    = $this->input->post('payment_mode');
        $discount        = $this->input->post('discount');
        $costPrice       = $this->input->post('cost_price');
        $serviceTaken    = $this->input->post('service_taken');

        $res_invo_id = $this->cm->insertInvoice($client_id, $customerId, $amountReceive, $payment_mode, $discount, $costPrice, $serviceTaken);

        if ($res_invo_id) {

            if($customerId) {

                $res1 = $this->db->where('id', $client_id)
                    ->select('sender_id')
                    ->get('client');

                $res2 = $this->db->where(array('client_id' => $client_id, 'id' => $customerId))
                    ->select('mob')
                    ->get('customers');

                if ($res1->num_rows() && $res2->num_rows()) {
                    $rows1 = $res1->result_array();
                    $rows2 = $res2->result_array();

                    $msg = urlencode('Dear Customer, Thanks for paying Rs. ' . $amountReceive . ', your invoice no, is ' . $res_invo_id . ' !\n Pls revisit soon!');
                    $sender_id = $rows1[0]['sender_id'];
                    $mob = $rows2[0]['mob'];

                    file_get_contents('http://www.anysms.in/api.php?username=anoopreselmix&password=696094&sender=' . $sender_id . '&sendto=' . $mob . '&message=' . $msg);
                }

            }
            echo json_encode( array('status'=>'200','message'=>'success') );
        }
        else{
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    /** Create new items category wise */
    public function saveItemsAndItsPrice() {
        $this->load->model('common_model', 'cm');

        $client_id  = $this->input->post('client_id');
        $categoryId = $this->input->post('cat_id');
        $itemName   = $this->input->post('item_name');
        $itemPrice  = $this->input->post('price');
        $hsnCode    = $this->input->post('hsn_code');
        $gstRate    = $this->input->post('gst_rate');

        $res = $this->cm->insertItemAndItsPrice($client_id, $categoryId, $itemName, $itemPrice, $hsnCode, $gstRate);

        if ($res == 1) {

            echo json_encode( array('status'=>'200','message'=>'success') );
        }
        else{
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }


    /** Getting the customer Data */
    public function gettingCustomerData() {
        $this->load->model('common_model', 'cm');
        $client_id = $this->input->get('client_id');

        $res = $this->cm->fetchingCustomerData($client_id);

        if ($res->num_rows()) {

            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);
        }
        else{
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    /** Getting Item's Category, Items and it's price using left join */
    public function gettingItemsDetails() {

        $query = '';

        $this->load->model('common_model', 'cm');

        if($this->input->get('query')) {
            $query = $this->input->get('query');
        }
        $client_id = $this->input->get('client_id');
        $res = $this->cm->fetchingItemsDetails($client_id, $query);

        if ($res->num_rows()) {

            $rows = $res->result_array();

            array_walk ( $rows, function (&$key) {
                $key['price'] = (int)$key['price']  + (int) (((int)$key['price'] * (int)$key['gst_rate'])/100);
            } );

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);
        }
        else{
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    /** Getting Users Details */

    public function gettingUsersDetails() {

        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->getting_user_details($data['client_id']);

        if ($res->num_rows()) {


            $rows = $res->result_array();

            array_walk ( $rows, function (&$key) {
                $key['password'] = base64_decode($key['password']);
            } );
            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);
        }
        else{
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }


    // create new user
    public function createUser() {
        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->create_user($data['client_id'], $data['name'], $data['email'], $data['password'], $data['active'], $data['admin']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }

    //update the user details
    public function updateUserDetails() {
        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->update_user($data['client_id'], $data['user_id'], $data['name'], $data['email'], $data['password'], $data['active'], $data['admin']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }

    public function deleteUserDetails() {
        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->delete_user($data['client_id'], $data['user_id']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }

    /** Fetching Invoice Data */

    public function gettingInvoiceDetails() {
        $this->load->model('common_model', 'cm');
        $client_id= $this->input->get('client_id');

        $res = $this->cm->fetchingInvoiceDetails($client_id);

        if ($res->num_rows()) {

            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);
        }
        else{
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    /** Getting the resources */

    public function gettingResources() {
        $this->load->model('Common_model', 'cm');
        $client_id = $this->input->get('client_id');

        $res = $this->cm->getResources($client_id);

        if ( $res->num_rows()) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }

    }


    // create new resources
    public function createResources() {
        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->create_new_resource($data['res_name'], $data['client_id']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }


    // update the resources
    public function updateResources() {
        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->update_resource($data['res_id'], $data['res_name'], $data['client_id']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }


    // delete the resources
    public function deleteResources() {
        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->update_resource($data['res_id'], $data['client_id']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }

    /** Getting Staff Details */

    public function gettingStaffList() {
        $this->load->model('Common_model', 'cm');
        $client_id  = $this->input->get('client_id');

        $res = $this->cm->get_staff_list($client_id);

        if ( $res->num_rows()) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }


    // create new staff
    public function createStaff() {
        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->create_new_staff($data['staff_name'], $data['client_id']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }

    // update the staff details
    public function updateStaff() {
        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->update_staff_details($data['staff_id'], $data['staff_name'], $data['client_id']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }

    // delete the staff details
    public function deleteStaff() {
        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->delete_staff_details($data['staff_id'], $data['client_id']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }

    /** Saving the appointment details */

    public function savingAppointmentDetails() {
        $this->load->model('common_model', 'cm');

        $data = json_decode(file_get_contents('php://input'), true);

        $res = $this->cm->insertAppointmentDetails($data['client_id'], $data['appointment']);

//        print_r($data['appointment']['0']['customerDetails']['id']);

        if ( $res->num_rows()) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            $res1 = $this->db->where('id', $data['client_id'])
                ->select('sender_id')
                ->get('client');

//            $res2 = $this->db->where('client_id', $data['client_id'])
//                ->select('appointment_msg')
//                ->get('message');

            $res3 = $this->db->where(array('client_id' => $data['client_id'], 'id' => $data['appointment']['0']['customerDetails']['id']))
                    ->select('mob')
                    ->get('customers');

            $msg = '';
            $sender_id = '';
            $mob = '';
            $book_date = $data['appointment']['0']['booked_date'];
            $book_time = $data['appointment']['0']['booked_time_24H'];

            if ( $res1->num_rows() && $res3->num_rows()) {
                $rows1 = $res1->result_array();
                $rows3 = $res3->result_array();

                $msg = urlencode('Dear Customer, Your Appointment is fixed with us on dated '.$book_date.', timing '.$book_time.' !\n Ur visit is awaited!');
                $sender_id = $rows1[0]['sender_id'];
                $mob = $rows3[0]['mob'];
            }

            file_get_contents('http://www.anysms.in/api.php?username=tripurari&password=6960&sender='.$sender_id.'&sendto='.$mob.'&message='.$msg);

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    // Fetch appointment details
    public function fetchAppointmentDetails() {
        $client_id  = $this->input->get('client_id');
        $this->load->model('common_model', 'cm');

        $res = $this->cm->fetch_appointment_details($client_id);

        if ( $res->num_rows()) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    /*********************** Customer Section (Add, Edit, Delete)
     ********************************************************************/
    // adding the customer
    public function saveCustomersDetails() {

        $client_id  = $this->input->post('client_id');
        $name       = $this->input->post('name');
        $email      = $this->input->post('email');
        $mob        = $this->input->post('mob');

        $this->load->model('common_model', 'cm');
        $res = $this->cm->storeCustomerDetails($client_id, $name, $email, $mob);

        if ($res == 1) {
              $res1 = $this->db->where('id', $client_id)
                           ->select('sender_id')
                          ->get('client');

              $res2 = $this->db->where('client_id', $client_id)
                                ->select('registration_msg')
                                ->get('message');

              $msg = '';
              $sender_id = '';

            if ( $res1->num_rows() && $res2->num_rows()) {
                $rows1 = $res1->result_array();
                $rows2 = $res2->result_array();

                $msg = urlencode($rows2[0]['registration_msg']);
                $sender_id = $rows1[0]['sender_id'];
            }

            file_get_contents('http://www.anysms.in/api.php?username=anoopreselmix&password=696094&sender='.$sender_id.'&sendto='.$mob.'&message='.$msg);

            echo json_encode( array('status'=>'200','message'=>'success') );
        }
        else{
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }

    }

    // updating the customer
    public function updateCustomerDetails() {

        $client_id  = $this->input->post('client_id');
        $id         = $this->input->post('id');
        $name       = $this->input->post('name');
        $email      = $this->input->post('email');
        $mob        = $this->input->post('mob');


        $this->load->model('common_model', 'cm');

        $res = $this->cm->update_customer_details($client_id, $id, $name, $email, $mob);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }

    // Delete customer details
    public function deleteCustomerDetails() {
        $id  = $this->input->post('id');
        $client_id  = $this->input->post('client_id');
        $this->load->model('common_model', 'cm');

        $res = $this->cm->delete_customer_details($client_id, $id);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }



    // save the general setting
    public function saveGeneralSetting() {
        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

         $res = $this->cm->save_general_setting($data['gs'], $data['client_id']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }

    // fetching general setting details
    public function fetchingGeneralSetting() {
        $client_id  = $this->input->get('client_id');
        $this->load->model('common_model', 'cm');

        $res = $this->cm->fetching_general_setting($client_id);

        if ( $res->num_rows() > 0) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }


    // Create new item (item name, item category, item price)

    public function createNewItem() {
        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->create_new_item($data['item_details'], $data['client_id']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }


    // update items details (item category, item, item rate)
    public function updateItemDetails() {

        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->update_item_details($data['item_details'], $data['client_id']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }


    // delete item
    public function deleteItemDetails() {
        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->delete_item_details($data['item_details'], $data['client_id']);


        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }


    // Message Setting Save && Update
    public function updateMsg() {
        $data = json_decode(file_get_contents('php://input'), true);

        $this->load->model('common_model', 'cm');

        $res = $this->cm->update_msg($data['reg_msg'], $data['bill_msg'], $data['appt_msg'], $data['client_id']);

        if ($res == 1) {
            echo json_encode(array('status' => '200', 'message' => 'success'));
        } else {
            echo json_encode(array('status' => '200', 'message' => 'fail'));
        }
    }


    // getting the message

    public function getMsg() {
        $client_id  = $this->input->get('client_id');
        $this->load->model('common_model', 'cm');

        $res = $this->cm->get_msg($client_id);

        if ( $res->num_rows() > 0) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }


    /** Report Section */

    // Sale Report
        public function getSaleReport() {
             $fromD       = $this->input->get('fd');
             $toD         = $this->input->get('td');
             $client_id   = $this->input->get('client_id');

            $this->load->model('common_model', 'cm');
            $res = $this->cm->get_sale_report($client_id, $fromD, $toD);

            if ( $res->num_rows() > 0) {
                $rows = $res->result_array();

                $response = array(
                    'status'    => 200,
                    'message'   => 'success',
                    'data'      => $rows
                );

                echo json_encode($response);

            } else {
                echo json_encode( array('status'=>'200','message'=>'fail') );
            }
        }


        // Appointment Report

        public function getAppointmentReport() {

            $fromD       = $this->input->get('fd');
            $toD         = $this->input->get('td');
            $client_id   = $this->input->get('client_id');

            $this->load->model('common_model', 'cm');

            $res = $this->cm->get_appointment_report($client_id, $fromD, $toD);

            if ( $res->num_rows() > 0) {
                $rows = $res->result_array();

                $response = array(
                    'status'    => 200,
                    'message'   => 'success',
                    'data'      => $rows
                );

                echo json_encode($response);

            } else {
                echo json_encode( array('status'=>'200','message'=>'fail') );
            }
        }


}