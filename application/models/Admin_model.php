<?php

class Admin_model extends CI_Model {

    // save client details
    public function saveClientDetails($c_name, $c_mob, $c_email, $sender_id, $is_active, $is_trial) {

        $plan_start = new DateTime(date('Y-m-d'));
        $plan_start = $plan_start->format('Y-m-d');

        if($is_trial == 1) {
            $plan_end = new DateTime(date('Y-m-d')); // Y-m-d
            $plan_end->add(new DateInterval('P30D'));
            $plan_end = $plan_end->format('Y-m-d');
        }
        else {
            $plan_end = new DateTime(date('Y-m-d')); // Y-m-d
            $plan_end->add(new DateInterval('P365D'));
            $plan_end = $plan_end->format('Y-m-d');
        }

        $data = array(
          'client_name'    =>  $c_name,
          'client_mob'     =>  $c_mob,
          'client_email'   =>  $c_email,
          'sender_id'      =>  $sender_id,
          'active'         =>  $is_active,
          'trial_plan'     =>  $is_trial,
          'plan_start'     =>  $plan_start,
          'plan_end'       =>  $plan_end,
          'created_at'     =>  date('Y-m-d H:i:s')
        );

        $res =  $this->db->insert('client', $data);
        if($res) {
            $client_id = $this->db->insert_id();
            if($client_id)
                return $this->db->insert('users', array('client_id' => $client_id, 'name' => $c_name, 'email' => $c_email, 'password' => base64_encode('admin'), 'active' => 1, 'admin' => 1, 'created_at' => date('Y-m-d H:i:s')));
        }
        else {
            return 0;
        }
    }



    // Fetch client details
    public function fetch_client_details() {
        return $this->db->get('client');
    }


    // Delete client details
    public function delete_client_details($id) {
        $this->db->where('id', $id);
        return $this->db->delete('client');
    }


    // update client details
    public function update_ClientDetails($id, $c_name, $c_mob, $c_email, $sender_id, $is_trial) {

        $plan_start = new DateTime(date('Y-m-d'));
        $plan_start = $plan_start->format('Y-m-d');

        if($is_trial == 1) {
            $plan_end = new DateTime(date('Y-m-d')); // Y-m-d
            $plan_end->add(new DateInterval('P30D'));
            $plan_end = $plan_end->format('Y-m-d');
        }
        else {
            $plan_end = new DateTime(date('Y-m-d')); // Y-m-d
            $plan_end->add(new DateInterval('P365D'));
            $plan_end = $plan_end->format('Y-m-d');
        }

        $data = array(
            'client_name'    =>  $c_name,
            'client_mob'     =>  $c_mob,
            'client_email'   =>  $c_email,
            'sender_id'      =>  $sender_id,
            'trial_plan'     =>  $is_trial,
            'plan_start'     =>  $plan_start,
            'plan_end'       =>  $plan_end,
            'updated_at'     =>  date('Y-m-d H:i:s')
        );

        $this->db->where('id', $id);

        return $this->db->update('client', $data);

    }

    // disable the client
    public function disable_client($id) {

        $this->db->select('active')
                  ->where('id', $id)
                  ->from('client');

       $res = $this->db->get();

       if($res->num_rows() > 0) {
          $row = $res->result_array();

          if($row[0]['active'] == 1) {
              $data = array(
                 'active' => 0
             );
          }
          else {
              $data = array(
                'active' => 1
                );
          }

           return $this->db->update('client', $data);
       }
       else {
           return;
       }

    }


    // fetch the users details
    public function fetch_user_details() {
        $this->db->select('c.client_name, u.id, u.email, u.password, u.active, u.admin',false);
        $this->db->from('users as u');
        $this->db->join('client as c','c.id = u.client_id', 'left');
         $res = $this->db->get();
         if($res->num_rows() > 0) {
             $rows = $res->result_array();

             array_walk ( $rows, function (&$key) {
                 $key['password'] = base64_decode($key['password']);
             } );

             return $rows;
         }
    }
}