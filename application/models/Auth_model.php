<?php

 class Auth_model extends CI_Model {

    public function admin_login($email, $password) {

        $this->db->where('u.email', $email);
        $this->db->where('u.password', $password);
        $this->db->where('u.active', 1);
        $this->db->from('users as u');

        return  $this->db->get();
    }

    public function last_login($id) {

        $data = array(
            'last_login' => date('Y-m-d H:i:s')
        );

        $this->db->where('id', $id);
        $chk = $this->db->update('users', $data);

        if($chk == 1) {
            $this->db->select('u.client_id, c.client_name, c.trial_plan, c.plan_start, c.plan_end, u.id, u.name, u.email, u.active, u.admin, u.last_login',false);
            $this->db->from('users as u');

            $this->db->join('client as c','c.id = u.client_id', 'left');
            $this->db->where('u.id', $id);

            return $this->db->get();
        }
        else {
            return;
        }
    }

    /************ Super Admin Login (Modal)
     ******************************************************/

    public function super_admin_login($email, $password) {

        $this->db->where('s.email', $email);
        $this->db->where('s.password', $password);
        $this->db->from('super_admin as s');

        return  $this->db->get();
    }
 }
