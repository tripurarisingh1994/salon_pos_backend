<?php

 class Common_model extends CI_Model {

     public function fetchFoodCategory($client_id) {

         return
             $this->db->where('client_id', $client_id)
                      ->get('food_category');
     }


     // create the food category
     public function  create_food_category($client_id, $cat_name) {
         return $this->db->insert('food_category', array('client_id' => $client_id,'name' => $cat_name, 'created_at' => date('Y-m-d H:i:s')));
     }

     // update the food category
     public function update_food_category($client_id, $cat_id, $cat_name) {

         $this->db->where(array('id' => $cat_id, 'client_id' => $client_id));

        return $this->db->update('food_category', array('name' => $cat_name, 'updated_at' => date('Y-m-d H:i:s')));

     }

     // delete the food category
     public function delete_food_category($client_id, $cat_id) {
         $this->db->where(array('id' => $cat_id, 'client_id' => $client_id));
         return $this->db->delete('food_category');
     }

     /** Fetching data from food-item table by food category id*/

     public function fetchFoodItemsByFoodCatId($id, $client_id) {

        return $this->db->where(array('client_id' => $client_id, 'food_category_id' => $id))
                        ->get('food_item');
     }

     /** Fetching thr price of food item by food item id*/

     public function fetchFoodItemPriceByFoodItemId($id, $client_id){

         return $this->db->where(array('fir.client_id' => $client_id,'fir.food_item_id' => $id))
                        ->select('fi.id as item_id, fi.hsn_code, fi.gst_rate, fir.id as price_id, fir.price as price')
                        ->from('food_item_rate as fir')
                        ->join('food_item as fi','fir.food_item_id=fi.id', 'left')
                        ->get();
     }

     /** Storing Customer Details in customers table */
     public function storeCustomerDetails($client_id, $name, $email, $mob) {

         $data = array(
           'client_id'  =>  $client_id,
           'name'       =>  $name,
           'email'      =>  $email,
           'mob'        =>  $mob,
           'created_at' =>  date('Y-m-d H:i:s'),
         );

         return $this->db->insert('customers', $data);
     }

     // Updating Customer Details
     public function update_customer_details($client_id, $id, $name, $email, $mob) {

         $data = array(
             'name'         => $name,
             'email'        => $email,
             'mob'          => $mob,
             'updated_at'   => date('Y-m-d H:i:s')
         );

         $this->db->where(array('id' => $id, 'client_id' => $client_id));

         return $this->db->update('customers', $data);

     }

     // Delete Customer Details
     public function delete_customer_details($client_id, $id) {
         $this->db->where(array('id' => $id, 'client_id' => $client_id));
         return $this->db->delete('customers');
     }





     /** Searching the customer by customer name */

     public function fetchCustomerByName($client_id, $query) {

         return $this->db->where('client_id', $client_id)
                          ->like('name', $query)
                          ->get('customers');

     }

     /** Searching the food items by item name */

     public function fetchFoodItemsByName($client_id, $query) {

         return $this->db->where('client_id', $client_id)
                         ->like('name', $query)
                         ->get('food_item');
     }

     /** Insert new Invoice Data */

     public function insertInvoice($client_id, $customerId, $amountReceive, $payment_mode, $discount, $costPrice, $serviceTaken) {

         $data = array(
             'client_id'      =>  $client_id,
             'customer_id'    =>  $customerId,
             'service_taken'  =>  $serviceTaken,
             'cost'           =>  $costPrice,
             'discount'       =>  $discount,
             'payment_mode'   =>  $payment_mode,
             'amount_receive' =>  $amountReceive,
             'created_at'     =>  date('Y-m-d H:i:s'),
         );

         $chk = $this->db->insert('invoice', $data);
         if($chk == 1) return $this->db->insert_id();
         else return false;
    }

    /** Insert New Item category wise and It's price */

    public function insertItemAndItsPrice($client_id, $categoryId, $itemName, $itemPrice, $hsnCode, $gstRate) {

        $data = array(
            'client_id'         => $client_id,
            'food_category_id'  => $categoryId,
            'name'              => $itemName,
            'hsn_code'          => $hsnCode,
            'gst_rate'          => $gstRate,
            'created_at'        => date('Y-m-d H:i:s')
        );

        $chk = $this->db->insert('food_item', $data);

        if($chk == 1) {

            $insertId = $this->db->insert_id();

            $data1 = array(
                'client_id'     =>  $client_id,
                'food_item_id'  =>  $insertId,
                'price'         =>  $itemPrice,
                'created_at'    =>  date('Y-m-d H:i:s')
            );

            return $this->db->insert('food_item_rate', $data1);
        }

    }

    /** Fetching  Customer Details */
    public function fetchingCustomerData($client_id) {
        $this->db->where('client_id', $client_id);
        return $this->db->get('customers');
    }

    /** Fetching Items Details */
    public function fetchingItemsDetails($client_id, $query) {
        $this->db->select('f.id as srn, c.id as category_id, c.name as category_name, f.id as item_id, f.name as item_name, f.hsn_code, f.gst_rate, r.id as price_id, r.price as price',false);
              $this->db->from('food_item as f');
              $this->db->where(array('f.client_id' => $client_id, 'c.client_id' => $client_id, 'r.client_id' => $client_id));
              if($query != '') {
                 $this->db->like('f.name', $query);
              }
              $this->db->join('food_category as c','c.id=f.food_category_id', 'left');
              $this->db->join('food_item_rate as r', 'f.id=r.food_item_id', 'left');

         return $this->db->get();
    }

    /** Fetching Users Details */
    public function fetchingUsersDetails() {
        return $this->db->get('users');
    }

    /** Fetching Invoice Details */

    public function fetchingInvoiceDetails($client_id) {
        return $this->db->where('in.client_id', $client_id)
                        ->select('c.name, c.email, c.mob, in.total_price, in.created_at, in.updated_at')
                        ->from('invoice as in')
                        ->join('customers as c', 'in.customer_id=c.id', 'left')
                        ->get();
    }

     /** Fetching the resources */

     public function getResources($client_id) {
         return $this->db->where('client_id', $client_id)
                         ->get('resources');
     }

     // create new resources
     public function create_new_resource($res_name, $client_id){
         return $this->db->insert('resources', array('client_id' => $client_id,'res_name' => $res_name, 'created_at' => date('Y-m-d H:i:s')));
     }

     // update the resources
     public function update_resource($res_id, $res_name, $client_id){
         $this->db->where(array('client_id' => $client_id, 'id' => $res_id));
         return $this->db->update('resources', array('res_name' => $res_name, 'updated_at' => date('Y-m-d H:i:s')));
     }

     // delete the resources
     public function delete_resource($res_id, $client_id){
         $this->db->where(array('client_id' => $client_id, 'id' => $res_id));
         return $this->db->delete('resources');
     }

     /** Fetching The Staff List */

     public function get_staff_list($client_id) {
         return $this->db->where('client_id', $client_id)
                         ->get('staff');
     }

     // create staff
     public function create_new_staff($staff_name, $client_id) {
         return $this->db->insert('staff', array('client_id' => $client_id,'staff_name' => $staff_name, 'created_at' => date('Y-m-d H:i:s')));
     }

     //update staff details
     public function update_staff_details($staff_id, $staff_name, $client_id) {
         $this->db->where(array('client_id' => $client_id, 'id' => $staff_id));
         return $this->db->update('staff', array('staff_name' => $staff_name, 'updated_at' => date('Y-m-d H:i:s')));
     }

     //delete staff details
    public function delete_staff_details($staff_id, $client_id) {
        return $this->db->where(array('client_id' => $client_id, 'id' => $staff_id))
                        ->delete('staff');
    }


     /** Insert appointment details */

     public function insertAppointmentDetails($client_id, $appointment) {

         foreach ($appointment as $_appnt) {

             $data = array(
                 'client_id'         => $client_id,
                 'customer_id'       => $_appnt['customerDetails']['id'],
                 'service_cate_id'   => $_appnt['serviceDetails']['category_id'],
                 'service_item_id'   => $_appnt['serviceDetails']['item_id'],
                 'service_price_id'  => $_appnt['serviceDetails']['price_id'],
                 'staff_id'          => $_appnt['staff_id'],
                 'resource_id'       => $_appnt['resource_id'],
                 'serv_start_time'   => $_appnt['servicing_startTime'],
                 'serv_end_time'     => $_appnt['servicing_endTime'],
                 'booked_date'       => $_appnt['booked_date'],
                 'booked_time_24H'   => $_appnt['booked_time_24H'],
                 'created_at'        => date('Y-m-d H:i:s')
             );

             $this->db->insert('appointment_details', $data);
        }

         return $this->fetch_appointment_details($client_id);
     }

     /** Fetch the Appointment Details */
     public function fetch_appointment_details($client_id) {

         return $this->db->select('c.name as cust_name, c.email as cust_email, c.mob as cust_mob, ap.id as appointment_id, ap.serv_start_time, ap.serv_end_time, ap.booked_date, ap.booked_time_24H, fc.name as serv_category_name, fi.name as service_name, fir.price as service_price, s.staff_name, r.res_name')
             ->where('ap.client_id', $client_id)
             ->from('appointment_details as ap')
             ->join('customers as c', 'ap.customer_id = c.id', 'left')
             ->join('food_category as fc', 'ap.service_cate_id = fc.id', 'left')
             ->join('food_item as fi', 'ap.service_item_id = fi.id', 'left')
             ->join('food_item_rate as fir', 'ap.service_price_id = fir.id', 'left')
             ->join('staff as s', 'ap.staff_id = s.id', 'left')
             ->join('resources as r', 'ap.resource_id = r.id', 'left')
             ->get();
     }



     // save the general setting
     public function save_general_setting($gs, $client_id) {

         $data = array(
             'client_id'   => $client_id,
             'shop_name'   => $gs['shopname'],
             'mobile'      => $gs['mobile'],
             'address'     => $gs['address'],
             'pin_code'    => (int)$gs['pincode'],
             'website'     => $gs['website'],
             'created_at'  => date('Y-m-d H:i:s'),
         );

         $data1 = array(
             'shop_name'   => $gs['shopname'],
             'mobile'      => $gs['mobile'],
             'address'     => $gs['address'],
             'pin_code'    => (int)$gs['pincode'],
             'website'     => $gs['website'],
             'updated_at'  => date('Y-m-d H:i:s'),
         );

             $this->db->where('client_id', $client_id);
             $query = $this->db->get('general_setting');

             if ($query->num_rows() > 0){
                 return $this->db->update('general_setting', $data1);
             }
             else{
                 return $this->db->insert('general_setting', $data);
             }


     }


     public function fetching_general_setting($client_id) {
         return $this->db->where('client_id', $client_id)
             ->get('general_setting');
     }

     // save item details

     public function create_new_item($item, $client_id) {

            $chk2 = $this->db->insert('food_item', array('client_id' => $client_id, 'food_category_id' => $item['category_id'], 'name' => $item['item_name'], 'hsn_code' => $item['hsn_code'], 'gst_rate' => $item['gst_rate'], 'created_at' => date('Y-m-d H:i:s')));

            if($chk2 == 1) {
                $insert_id1 = $this->db->insert_id();

                return $this->db->insert('food_item_rate', array('client_id' => $client_id, 'food_item_id' => $insert_id1, 'price' => $item['price'], 'created_at' => date('Y-m-d H:i:s')));
            }

     }

     // update the item details

     public function update_item_details($item, $client_id) {

             $this->db->where(array('id' => $item['item_id'], 'food_category_id' => $item['category_id'], 'client_id' => $client_id));

             $res1 = $this->db->update('food_item', array( 'food_category_id' => $item['category_id'], 'name' => $item['item_name'],'hsn_code' => $item['hsn_code'], 'gst_rate' => $item['gst_rate'],'updated_at' => date('Y-m-d H:i:s')));

             if($res1 == 1) {

                 $this->db->where(array('id' => $item['price_id'], 'food_item_id' => $item['item_id'], 'client_id' => $client_id));

                 return $this->db->update('food_item_rate', array('price' => $item['price'], 'updated_at' => date('Y-m-d H:i:s')));
             }

     }


     // delete item details
     public function delete_item_details($item, $client_id) {

         $this->db->where(array('id' => $item['item_id'], 'food_category_id' => $item['category_id'], 'client_id' => $client_id));
         $chk = $this->db->delete('food_item');

         if($chk == 1) {
             $this->db->where(array('id' => $item['price_id'], 'food_item_id' => $item['item_id'], 'client_id' => $client_id));
             return $this->db->delete('food_item_rate');
         }
     }


     // getting user details
     public function getting_user_details($client_id) {
         return $this->db->get_where('users', array('client_id' => $client_id));
     }

     // create user
     public function create_user($client_id, $name, $email, $password, $active, $admin) {
         return $this->db->insert('users', array('client_id' => $client_id,'name' => $name, 'email' => $email, 'password' => base64_encode($password), 'active' => $active, 'admin' => $admin, 'created_at' => date('Y-m-d H:i:s')));
     }

     // update the user details
     public function update_user($client_id, $id, $name, $email, $password, $active, $admin) {
         return
             $this->db->where(array('id' => $id, 'client_id' => $client_id))
                         ->update('users', array('name' => $name, 'email' => $email, 'password' => base64_encode($password), 'active' => $active, 'admin' => $admin, 'updated_at' => date('Y-m-d H:i:s')));
     }

     // delete user details
     public function delete_user($client_id, $user_id) {
         return
                $this->db->where(array('client_id' => $client_id, 'id' => $user_id))
                          ->delete('users');
     }

     // update the msg
     public function update_msg($reg_msg, $bill_msg, $appt_msg, $client_id) {

         $data = array(
           'client_id'          => $client_id,
           'registration_msg'   => trim($reg_msg),
           'bill_msg'           => trim($bill_msg),
           'appointment_msg'    => trim($appt_msg),
           'created_at'         => date('Y-m-d H:i:s')
         );

         $data1 = array(
             'registration_msg'   => trim($reg_msg),
             'bill_msg'           => trim($bill_msg),
             'appointment_msg'    => trim($appt_msg),
             'updated_at'         => date('Y-m-d H:i:s')
         );


         $this->db->where('client_id', $client_id);
         $query = $this->db->get('message');

         if ($query->num_rows() > 0){
             return $this->db->update('message', $data1);
         }
         else{
             return $this->db->insert('message', $data);
         }
     }


     // getting the msg
     public function get_msg($client_id) {
         return $this->db->where('client_id', $client_id)
             ->get('message');
     }



     /**  Report Section */


     // sale report
     public function get_sale_report($client_id, $fromD, $toD) {
         return $this->db->where(array('in.client_id' => $client_id, 'in.created_at >=' => date('Y-m-d', strtotime($fromD)), 'in.created_at <=' => date('Y-m-d', strtotime($toD))))
             ->select('c.name as customer_name, c.mob as customer_mob, in.service_taken, in.cost as cost_price, in.discount, in.tax, in.amount_receive')
             ->from('invoice as in')
             ->join('customers as c', 'in.customer_id=c.id', 'left')
             ->get();
     }


     // Appointment Report
     public function get_appointment_report($client_id, $fromD, $toD) {
         return $this->db->where(array('ap.client_id' => $client_id, 'ap.created_at >=' => date('Y-m-d', strtotime($fromD)), 'ap.created_at <=' => date('Y-m-d', strtotime($toD))))
                ->select('c.name as customer_name, c.mob as customer_mob, f.name as service_name, fr.price as service_price, ,s.staff_name, ap.created_at')
                ->from('appointment_details as ap')
                ->join('customers as c', 'ap.customer_id = c.id', 'left')
                ->join('food_item as f', 'ap.service_item_id = f.id', 'left')
                ->join('food_item_rate as fr', 'ap.service_price_id = fr.id', 'left')
                ->join('staff as s', 'ap.staff_id = s.id', 'left')
                ->get();
     }
 }
 