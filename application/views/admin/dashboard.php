<?php require_once 'header.php'; ?>
</head>
<body>
<p hidden id="base-path"><?=base_url();?></p>
<div id="wrapper">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a>
                    Salon Clients
                </a>
            </li>
            <li>
                <a href="#">Dashboard</a>
            </li>
            <li>
                <a href="#" id="add-client">Clients</a>
            </li>
            <li>
                <a href="#" id="users">Users</a>
            </li>
            <li>
                <a href="<?= base_url();?>logout">Logout</a>
            </li>
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <a href="#menu-toggle" class="btn btn-secondary" id="menu-toggle"><i class="fas fa-bars"></i></a>
            <div id="main-content"></div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<script src="<?=base_url(); ?>assets/js/jquery.min.js"></script>

<script>

    const base_path = $('#base-path').text();

    let flag = true;   // for edit and upload client
    /********** Delete Client Details
     *
     * @param id
     */
    function deleteClient(id) {

        $.ajax({
            url: base_path+"delete-client",
            method: "POST",
            data: {id},
            success: function (data) {
                // console.log(data)
                const _data = JSON.parse(data)

                if(_data['message'] == 'success') {
                    $.get(base_path+"view-client", function(data, status){
                        $('#main-content').html(data);
                    });
                }
                else {
                    alert('Deletion Fail!')
                }
            }
        });
    }


    /****************** Edit Client Details
     *
     * @param id
     */
    function editClient(id) {

        var selectors = [
            '#c_name' + id,
            '#c_mob' + id,
            '#c_email' + id,
            '#is_trial' + id,
            '#sender_id' +id
        ];

        if(flag) {
            $(selectors.join(", ")).prop("disabled", false);

            $('#edit'+id+' i:first-child')
                .removeClass("fa-edit")
                .addClass("fa-upload");

            $('#edit'+id).attr("title", "upload the details");

            flag = false
        }
        else {

            $.ajax({
                url: base_path+"update-client",
                method: "POST",
                data: {id, c_name: $('#c_name'+id).val(), c_mob: $('#c_mob'+id).val(), c_email: $('#c_email'+id).val(), sender_id: $('#sender_id'+id).val(), is_trial: $('#is_trial'+id).val()},
                success: function (data) {
                    // console.log(data)
                    const _data = JSON.parse(data)

                    if(_data['message'] == 'success') {
                        alert('updation successful');
                        $.get(base_path+"view-client", function(data, status){
                            $('#main-content').html(data);
                        });
                    }
                    else {
                        alert('updation Fail!')
                    }
                }
            });







            $(selectors.join(", ")).prop("disabled", true);

            $('#edit'+id+' i:first-child')
                .removeClass("fa-upload")
                .addClass("fa-edit");

            $('#edit'+id).attr("title", "Edit the client");

            flag = true
        }

    }

    /****************** Enable or Disable the client
     *
     * @param id
     */
    function enable_or_disable_client(id) {
        console.log(id);

        $.ajax({
            url: base_path+"disable-client",
            method: "POST",
            data: {id},
            success: function (data) {
                // console.log(data)
                const _data = JSON.parse(data)

                if(_data['message'] == 'success') {
                    $.get(base_path+"view-client", function(data, status){
                        $('#main-content').html(data);
                    });
                }
                else {
                    alert('Fail!')
                }
            }
        });
    }

</script>

<?php require_once 'footer.php'; ?>

