$( document ).ready(function() {

    /****************************** admin page (login page)
    *************************************************************************/

    $('#login-btn').click(function () {

        const base_path = $('#base-path').text();        // Getting Base Path

        const email = $('#uname').val();
        const password  = $('#psw').val();


        if(email && password) {


            $.ajax({
                url: base_path+"index.php/auth/super_admin",
                method: "POST",
                data: {email, password},
                success: function (data) {
                    console.log(data)
                    const _data = JSON.parse(data);

                    if(_data['message'] == 'success') {
                        alert('Login successful');
                        window.location.href = base_path+'index.php/admin/dashboard'
                    }
                    else {
                        alert('Login fail! Plz try again')
                    }
                }
            });


        }

    });


    /*********************************** dashboard page
     ***********************************************************************/

    const base_path = $('#base-path').text();        // Getting Base Path

    <!-- Menu Toggle Script -->
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

                /*************** Add Client (at dashboard page) *************/

                $('#add-client').click(function () {

                    $.get(base_path+"addClient", function(data, status){

                        $('#main-content').html(data);
                    });

                });

                $("#main-content").on("click", "#add-client-btn", function () {
                    $.get(base_path+"addClient", function(data, status){

                        $('#main-content').html(data);
                    });
                });

                $("#main-content").on("click", "#submit", function () {
                    // console.log('submit btn')

                    const c_name    = $('#c-name').val();
                    const c_mob     = $('#c-mob').val();
                    const c_email   = $('#c-email').val();
                    const sender_id = $('#sender-id').val();
                    const is_active = $('#isactive').val();
                    const is_trial  = $('#trial-30').val();

                    $.ajax({
                        url: base_path+"submit-client-details",
                        method: "POST",
                        data: {c_name, c_mob, c_email, sender_id, is_active, is_trial},
                        success: function (data) {
                            // console.log(data)
                            const _data = JSON.parse(data);

                            if(_data['message'] == 'success') {
                                alert('Data added successful');
                                $('#contactFrm').trigger("reset");
                            }
                            else {
                                alert('Data adding fail! Plz try again')
                            }
                        }
                    });

                });


    /****************** View Client Details
     ***********************************************************/

    $("#main-content").on("click", "#view-client-btn", function () {

        $.get(base_path+"view-client", function(data, status){
            $('#main-content').html(data);
        });
    });



    // view user details
    $('#users').click(function () {

        $.get(base_path+"view-user", function(data, status){

            $('#main-content').html(data);
        });

    });

});